import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
class MyTableModel extends AbstractTableModel
{
    String[]columnNames;
    Object[][]data;
    int type;
    public MyTableModel(Object[][]data,String[]columns, int type)
    {
        this.type=type;
        this.data=data;
        columnNames=columns;
    }
    public int getColumnCount()
    {
        return columnNames.length;
    }
    public int getRowCount()
    {
        return data.length;
    }
    public String getColumnName(int col)
    {
        return columnNames[col];
    }
    public String[] getColumnNames()
    {
        return columnNames;        
    }
    public Object getValueAt(int row, int col)
    {
        return data[row][col];
    }
    public Object[] getRowValues(int r)
    {
        return data[r];
    }
    public Class getColumnClass(int c)
    {        
        //if(type==1)
        //{
            if(getValueAt(0,c)==null)
                return new String().getClass();
            return getValueAt(0,c).getClass();
        //}
        //else 
          //  return new String().getClass();
        
    }
    public Object[][] getData()
    {
        return data;
    }
    public void setData(Object [][] ndata)
    {
        data=ndata;
        fireTableDataChanged();
    }
    public void setValueAt(Object value, int row, int col)
    {
        data[row][col]=value;
        fireTableChanged(new TableModelEvent(this, row, row, col));
    }
    public boolean isCellEditable(int row, int column)
    {
        if(type==1)
            return column==2;
        else
            return false;
    } 
    public void insertRow(Object[]newRow)
    {
        Object nData[][]=new Object[getRowCount()+1][getColumnCount()];
        for(int i=1;i<getRowCount();i++)nData[i]=data[i-1];
        nData[0]=newRow;
        data=nData;     
    }
    public void removeRow(int row)
    {
        
        Object nData[][]=new Object[getRowCount()-1][getColumnCount()];
        for(int i=0;i<row;i++)
            nData[i]=data[i];
        for(int i=row;i<getRowCount()-1;i++)
            nData[i]=data[i+1];
        data=nData;
        fireTableRowsDeleted(row, row);
    }
    public void setRowValues(int row, Object[] newRow)
    {
        data[row]=newRow;
    }
    
}