import java.io.File;
import java.io.IOException;
import java.util.*;
 
import jxl.*;
import jxl.write.*;
import jxl.write.Boolean;
import jxl.write.Number;
import jxl.write.biff.RowsExceededException; 
public class JavaExcelWrite {
    File exlFile;
    WritableWorkbook writableWorkbook;
    Vector<WritableSheet> sheets;
    public JavaExcelWrite(String name)
    {    
        sheets=new Vector<WritableSheet>();
        try {
            File exlFile = new File(""+name+".xls");
            writableWorkbook = Workbook.createWorkbook(exlFile); 
            //WritableSheet writableSheet = writableWorkbook.createSheet("Sheet1", 0);
 
            //Create Cells with contents of different data types.
            //Also specify the Cell coordinates in the constructor
            /*Label label = new Label(0, 0, "Label (String)");
            DateTime date = new DateTime(1, 0, new Date());
            Boolean bool = new Boolean(2, 0, true);
            Number num = new Number(3, 0, 9.99);*/
 
            //Add the created Cells to the <span id="IL_AD7" class="IL_AD">sheet</span>
            /*writableSheet.addCell(label);
            writableSheet.addCell(date);
            writableSheet.addCell(bool);
            writableSheet.addCell(num);*/
 
            //Write and close the workbook
            //writableWorkbook.write();
            //writableWorkbook.close();
        } catch (IOException e) {
            e.printStackTrace();     }   
    }
    public void addSheet(String name,String [] titles, Object[][] data)
    {
        try
        {
            WritableSheet temp=writableWorkbook.createSheet(name,sheets.size());
            sheets.addElement(temp);
            for(int i=0;i<titles.length;i++)
            {
                temp.addCell(new Label(i,0,titles[i]));
            }
            for(int i=0;i<data.length;i++)
            {
                for(int j=0;j<data[i].length;j++)
                {
                    if(data[i][j] instanceof String)                    
                        temp.addCell(new Label(j,i+1,(String)data[i][j]));
                    else if(data[i][j] instanceof Integer)
                        temp.addCell(new Number(j,i+1,(Integer)data[i][j]));
                    else if(data[i][j] instanceof Boolean)
                    {
                        java.lang.Boolean bool=(java.lang.Boolean)data[i][j];
                        temp.addCell(new Boolean(j,i+1,bool));
                    }
                }
            }
            temp.addCell(new Formula(0,data.length+1,"SUM(A2:A4)"));
            writableWorkbook.write();
            writableWorkbook.close();
        }catch(WriteException e){System.out.println(e.getMessage());}
        catch(IOException e){System.out.println(e.getMessage());}
    }
}