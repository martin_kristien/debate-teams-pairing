import java.io.*;
public class DirectAccessFile
{
    private RandomAccessFile f;
    private long recordLength;
    private String filename;
    private int numOfRecords=0;
    //private boolean open; // useful if we close the file after every operation
    public DirectAccessFile(String filename, long recordLength)
    {
        this.filename = filename;
        this.recordLength = recordLength;
        try
        {            
            f = new RandomAccessFile(filename, "rwd");
            // rwd means that it writes to the main memory after each operation
        } catch(FileNotFoundException e)
        {
            System.out.println(e.getMessage());
        }
    }
    public void display()
    {
        for(int i=0;i<getSize();i++)
        {
            System.out.println(""+read(i));
        }
        System.out.println("");
    }
    public void close()
    {
        try
        {
            f.close();
        }catch(Exception e){}
    }
    public void setLength(long length)
    {
        try
        {
            f.setLength(length);
        }catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
    public long getRecordLength()
    {
        return recordLength;
    }
    public long getLength()
    {
        try
        {
            return f.length();
        }catch(IOException e)
        {
            return -1;
        }        
    }
    public String getFileName()
    {
        return filename;
    }
    public long getSize()
    {        
        try{
            return f.length()/recordLength;
        } catch(IOException e){
            System.out.println(e.getMessage());
        }
        return -1;
    }    
    public String read(long recordNumber){
        try{
            f.seek(recordNumber*recordLength);
            String temp=f.readUTF().trim();
            //System.out.println(temp);
            return temp;
        } catch(IOException e)
        {
            System.out.println("Exceptione:"+e.getMessage());
        }
        return null;
    }    
    public static long getUTFLength(String s){
        try{
            byte[] utfBytes = s.getBytes("UTF8");
            return utfBytes.length;
        } catch(UnsupportedEncodingException e){
            System.out.println(e.getMessage());
        }
        return -1;
    }
    public static String adjust(String s, long recordLength)
    {
        try
        {
            byte[] utfBytes=s.getBytes("UTF8");
            while(utfBytes.length>recordLength-2)
            {
                s=s.substring(0,s.length()-1);
                utfBytes=s.getBytes("UTF8");
            }      
            while(utfBytes.length!=recordLength-2)
            {
                s=s+" ";
                utfBytes=s.getBytes("UTF8");
            }
        }catch(UnsupportedEncodingException e)
        {
            System.out.println(""+e.getMessage());
        }        
        return s;
    }
    public void write(String s, int recordNumber)
    {
        try
        {
            f.seek(recordNumber*recordLength);
            f.writeUTF(adjust(s,recordLength));
            numOfRecords++;
        }catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
    public int getNumOfRecords()
    {
        return numOfRecords;
    }
    public void delete(int index)
    {
        write(read(getLength()-recordLength), index);
        try
        {
            f.setLength(getLength()-recordLength);
        }catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
    
}