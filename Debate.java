public class Debate
{
    private String a,n;
    private int ballots1,ballots2;
    private int speakersPoints=0;
    private String judge;
    private int index;
    public Debate(Team a, Team n, int index)
    {
        this.a=a.nameOfTeam;
        this.n=n.nameOfTeam;
        judge=new String();
        this.index=index;
    }
    public Debate(Team a,int position,int index)
    {
        if(position==0)
        {
            n=a.nameOfTeam;
            this.a="";
        }
        else
        {
            n="";
            this.a=a.nameOfTeam;
        }
        this.index=index;
    }    
    public Debate(String rafdata, int index)
    {
        this.index=index;
        String [] data=rafdata.split(";");
        a=data[0];
        n=data[1];
        String [] temp=data[2].split(",");
        ballots1=Integer.parseInt(temp[0]);
        ballots2=Integer.parseInt(temp[1]);
        speakersPoints=Integer.parseInt(data[3]);
        if(data.length==5) judge=data[4];
        else judge=new String();
    }
    public Object[] getRow()
    {
        Object[]temp=new Object[4];
        temp[0]=a;
        temp[1]=n;
        if(judge!=null)   temp[2]=judge;
        else    temp[2]=new String("");
        return temp;
    }
    public Object[] getResultsRow()
    {
        Object[]temp=new Object[3];
        temp[0]=a;
        temp[1]=n;
        temp[2]=haveResults();
        return temp;
    }
    public void addJudge(String name)
    {
        judge=name;
    }
    public void addResults(int ballots1, int ballots2)
    {
        this.ballots1=ballots1;
        this.ballots2=ballots2;
    }
    public String toString()
    {
        String temp=new String();
        String a=new String();
        String n=new String();
        String j=new String();
        if(this.a==null)
            a="";
        else a=this.a;
        if(this.n==null)n="";
        else n=this.n;
        if(judge==null)j="";
        else j=judge;
        temp=temp.concat(""+a+";"+n+";");
        temp=temp.concat(""+ballots1+","+ballots2+";");   
        temp=temp.concat(speakersPoints+";");   
        temp=temp.concat(""+j);
        return temp;
    }
    public boolean haveResults()
    {
        return ballots1!=0||ballots2!=0;
    }
    public String getA()
    {
        if(a==null)
            return "null";
        return a;
    }
    public int getIndex()
    {
        return index;
    }
    public String getN()
    {
        if(n==null)
            return "null";
        return n; 
    }
    public int getPosition()
    {
        if(a.equals(""))
            return 0;
        if(n.equals(""))
            return 1;
        return -1;
    }
    public String getTeam()
    {
        if(a.equals(""))
        return n;
        if(n.equals(""))
        return a;
        return null;
    }
    public void addSpeakersPoints(int p)
    {
        speakersPoints+=p;
    }
    public int getSpeakersPoints()
    {
        return speakersPoints;
    }
}