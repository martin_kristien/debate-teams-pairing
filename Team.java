import java.util.*;
class Team
{
    protected String nameOfTeam;
    protected Speaker [] speakers;
    protected int []nums;
    private char improSide=' ', proSide=' ';
    int index;
    public int special=0;
    public Team(String nameOfTeam, String [] s, int index)
    {
        this.nameOfTeam=nameOfTeam;
        speakers=new Speaker[s.length];
        for(int i=0;i<speakers.length;i++)
            speakers[i]=new Speaker(s[i],nameOfTeam);       
        nums=new int [3];
        this.index=index;
    }
    public Team(String rafdata, int index)
    {
        String [] data=rafdata.split("`");
        String [] team=data[0].split(";");
        nameOfTeam=team[0];
        String [] temp=team[1].split(",");
        nums=new int[] {Integer.parseInt(temp[0]),Integer.parseInt(temp[1]),Integer.parseInt(temp[2])};
        improSide=team[2].charAt(0);
        proSide=team[2].charAt(1);        
        this.index=index;
        speakers=new Speaker [data.length-1];
        for(int i=0;i<speakers.length;i++)
        {          
            temp=data[i+1].split(";");
            if(temp.length==1)
                continue;
            speakers[i]=new Speaker(temp[0],nameOfTeam,Integer.parseInt(temp[1]));            
        }
    }
    public Object[] getRow(boolean firstRound)
    {
        if(firstRound)
            {
            Object[] row=new Object[5];
            row[0]=nameOfTeam;
            for(int i=1;i<speakers.length+1;i++)
                row[i]=speakers[i-1].getName();
            if(speakers.length==3)
                row[4]=new String("");
            return row;
        }
        else
        {
            Object[] row=new Object[]{nameOfTeam,new Integer(nums[0]),new Integer(nums[1]),new Integer(nums[2])};
            return row;
        }
    }
    public String toString()
    {
        String temp=new String();
        temp=temp.concat(""+nameOfTeam+";");
        temp=temp.concat(""+nums[0]+","+nums[1]+","+nums[2]+";");
        temp=temp.concat(""+improSide+proSide+"`");        
        for(int i=0;i<speakers.length;i++)
            temp=temp.concat(""+speakers[i].toString()+"`");
        return temp;
    }
    public Speaker[] getSpeakers()
    {
        return speakers;
    }
    public Speaker getSpeaker(String name)
    {
        for(int i=0;i<speakers.length;i++)
        {
            if(speakers[i].getName().equals(name))
                return speakers[i];
        }
        return null;
    }
    public void addResults(int ballots)
    {
        if(ballots>=2)
            nums[0]++;
        nums[1]+=ballots;
        int temp=0;
        for(int i=0;true;i++)
        { 
            try
            {
                temp+=speakers[i].getPoints();
            }catch(IndexOutOfBoundsException e)
            {
                break;
            }
        }
        nums[2]=temp;
    }
    public void addBallots(int ballots)
    {
        if(ballots>=2)
            nums[0]++;
        nums[1]+=ballots;
    }
    public void addSpeakersPoints(int points)
    {
        nums[2]+=points;
    }
    public int[] getNums()
    {
        return nums;
    }
    public String getName()
    {
        return nameOfTeam;
    }
    public int getIndex()
    {
        return (index);
    }
    public int nextDebate(boolean impro)
    {
        if(impro)
        {
            if(improSide=='a')
                return 1;
            else if(improSide=='n')
                return 0;
            else return -1;
        }
        else
        {
            if(proSide=='a')
                return 1;
            else if(proSide=='n')
                return 0;
            else return -1;
        }
    }
    public void setPosition(char position, boolean impro)
    {
        if(impro)
        {
            improSide=position;
        }
        else
        {
            proSide=position;
        }
    }
}