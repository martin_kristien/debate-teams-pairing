import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.border.*;
import javax.swing.table.*;
public class GUI extends JFrame implements ActionListener, TableModelListener
{
    Vector<Team> teams;
    Vector<Debate> debates;
    Vector<Speaker>speakers;
    boolean firstRound;
    DirectAccessFile fileTeams,fileDebates;
    JavaExcelWrite excelWriter;    
    int averageSpeakerPoints;
    
    TableDemoPanel tablePanel,tablePairing,tableAddingResult,tableSpeakers;
    JLabel title,subtitle, LnameOfTeam,Lspeaker1,Lspeaker2,Lspeaker3,Lspeaker4,teamNameA,teamNameN;
    JButton addTeams,end,pair,addTeam,removeTeam,pairing,speakerChart,teamChart,addResults,updateDebate;
    JButton readDebateUpdate, cancelUpdate;
    JTextField speaker1,speaker2,speaker3,speaker4,nameOfTeam,ballotsA,ballotsN;
    JTextField[] speakerResults;
    JLabel[] speakerNames;
    JPanel results,panel;
    JCheckBox subst;
    Container cont;
    Screen firstScreen,mainScreen,addTeamsScreen,pairingScreen,addResultsScreen,speakersChartScreen;
    DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
    DefaultTableCellRenderer leftRenderer=new DefaultTableCellRenderer();
    public GUI()
    {      
        super("Debate Teams Pairing");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        fileTeams=new DirectAccessFile("teams",500);
        fileDebates=new DirectAccessFile("debates",200);
        teams=new Vector<Team>();
        debates=new Vector<Debate>();
        speakers=new Vector<Speaker>();                
        
        Object[] options = { "New Tournament", "Resume Tournament" };
        int option=JOptionPane.showOptionDialog(null, "Select Tournament Type", "Startup",
        JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
        null, options, options[0]);
        
        setSize(800,700);
        setResizable(false);
        setVisible(true);
        setLayout(null);
        cont=getContentPane();
        mainScreen=new Screen();
        pairingScreen=new Screen();
        firstScreen=new Screen();
        addTeamsScreen=new Screen();
        addResultsScreen=new Screen();
        speakersChartScreen=new Screen();
        
        
        panel = new JPanel();
        panel.setLayout(null);
        //panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        panel.setBackground(Color.WHITE);
        panel.setBounds(0, 336, 734, 124);
        panel.setVisible(false);
        
        JTextField textField = new JTextField();
        textField.setColumns(10);
        textField.setBounds(303, 11, 37, 20);
        panel.add(textField);
        if(panel==null)
            System.out.println("null only");
        cont.add(panel);
        
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
        leftRenderer.setHorizontalAlignment(JLabel.LEFT);
        
        title=new JLabel("PAIRING",JLabel.CENTER);
        title.setBounds(0,0,800,70);
        cont.add(title);
        title.setOpaque(true);
        title.setBackground(new Color(135, 206, 250));
        title.setForeground(new Color(0,0,0));
        title.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
        title.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 24));   
        subtitle=new JLabel("Menu",JLabel.CENTER);
        subtitle.setBounds(250,100,300,50);
        subtitle.setFont(new Font("Times New Roman", Font.BOLD, 20));
        cont.add(subtitle);
        end=new JButton("End");
        end.setBounds(600,570,150,50);
        cont.add(end);
        end.addActionListener(this);
        
        tablePairing=new TableDemoPanel(new Object[0][3],new String[]{new String("Affirmative"),new String("Negative"),new String("Referee")},1);
        tablePairing.setOpaque(true);
        cont.add(tablePairing);
        tablePairing.setBounds(10,200,770,350);
        pairingScreen.addObject(tablePairing);  
        tablePairing.setVisible(false);
        tablePairing.setColumnSelection(2,2);
        tablePairing.getModel().addTableModelListener(this);
        
        pairing=new JButton("Pairing");
        pairing.setBounds(150,200,200,50);
        cont.add(pairing);
        pairing.addActionListener(this);
        mainScreen.addObject(pairing);
        
        speakerChart=new JButton("Speakers list");
        speakerChart.setBounds(150,300,200,50);
        cont.add(speakerChart);
        speakerChart.addActionListener(this);        
        mainScreen.addObject(speakerChart);
        
        teamChart=new JButton("Team list");
        teamChart.setBounds(150,400,200,50);
        cont.add(teamChart);
        teamChart.addActionListener(this);
        mainScreen.addObject(teamChart);
        
        addResults=new JButton("Add results");
        addResults.setBounds(150,500,200,50);
        cont.add(addResults);
        addResults.addActionListener(this);
        mainScreen.addObject(addResults);
        
        tableSpeakers=new TableDemoPanel(new Object[0][4],new String[]{new String("#"),new String("Speaker's name"),new String("points"),new String("Team")},2);
        tableSpeakers.setBounds(10,200,770,350);
        tableSpeakers.setOpaque(true);
        tableSpeakers.setVisible(false);
        cont.add(tableSpeakers);
        speakersChartScreen.addObject(tableSpeakers);
        tableSpeakers.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
        tableSpeakers.getColumnModel().getColumn(2).setCellRenderer(leftRenderer);
        //tableSpeakers.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tableSpeakers.getColumnModel().getColumn(0).setMaxWidth(20);
        
        tableAddingResult=new TableDemoPanel(new Object[0][3],new String[]{new String("Affirmative"),new String("Negative"), new String("Results added")},2);
        tableAddingResult.setBounds(10,200,770,350);
        tableAddingResult.setOpaque(true);
        tableAddingResult.setVisible(false);
        cont.add(tableAddingResult);
        addResultsScreen.addObject(tableAddingResult);
        
        results=new JPanel(null);
        results.setLayout(null);
        results.setBounds(10,400,770,200);       
        results.setBackground(getBackground());
        cont.add(results);
        results.setVisible(false);
        speakerResults=new JTextField[8];
        speakerNames=new JLabel[8];
        for(int i=0;i<8;i++)
        {
            speakerResults[i]=new JTextField();
            speakerResults[i].setBounds(97*i,70,90,30);
            //speakerResults[i].setVisible(true);
            results.add(speakerResults[i]);
            speakerNames[i]=new JLabel("Empty");
            speakerNames[i].setBounds(97*i,50,90,20);
            results.add(speakerNames[i]);
        }
        readDebateUpdate=new JButton("Load Results");
        readDebateUpdate.setBounds(0,110,380,30);
        results.add(readDebateUpdate);
        readDebateUpdate.addActionListener(this);
        
        cancelUpdate=new JButton("Cancel Selection");
        cancelUpdate.setBounds(390,110,380,30);
        results.add(cancelUpdate);
        cancelUpdate.addActionListener(this);
        
        teamNameA=new JLabel("affirmative",JLabel.RIGHT);
        teamNameA.setBounds(300,0,80,20);
        results.add(teamNameA);
        teamNameN=new JLabel("negative",JLabel.LEFT);
        teamNameN.setBounds(390,0,80,20);
        results.add(teamNameN);
        
        ballotsA=new JTextField();
        ballotsA.setBounds(330,20,50,20);
        results.add(ballotsA);
        ballotsN=new JTextField();
        ballotsN.setBounds(390,20,50,20);
        results.add(ballotsN);
        
        JLabel temp=new JLabel(":",JLabel.CENTER);
        temp.setBounds(380,20,10,20);
        results.add(temp);
        
        updateDebate=new JButton("Add results");
        updateDebate.setBounds(600,150,150,30);
        updateDebate.setVisible(false);
        cont.add(updateDebate);
        addResultsScreen.addObject(updateDebate);
        updateDebate.addActionListener(this);
        
        pair=new JButton("Make Pairing");
        pair.setBounds(300,400,200,50);
        cont.add(pair);
        pair.addActionListener(this);
        firstScreen.addObject(pair);
        mainScreen.addObject(pair);
    
        mainScreen.setVisible(false);
        if(option==0)
        {
            fileTeams.setLength(0);
            fileDebates.setLength(0);
            firstRound=true;
            addTeams=new JButton("Add Team");
            addTeams.setBounds(300,300,200,50);
            cont.add(addTeams);
            addTeams.addActionListener(this);
            firstScreen.addObject(addTeams);
            
            
            tablePanel=new TableDemoPanel(new Object[1][5],new String[]{new String("Team name"),new String("Speaker 1"),new String("Speaker 2"),new String("Speaker 3"),new String("Substitution")},0);
            tablePanel.setOpaque(true);
            cont.add(tablePanel);
            tablePanel.setBounds(10,300,770,250);
            tablePanel.setVisible(false);
            addTeamsScreen.addObject(tablePanel);            
            //154
            nameOfTeam=new JTextField();
            nameOfTeam.setBounds(10,200,150,20);
            cont.add(nameOfTeam);
            nameOfTeam.setVisible(false);
            addTeamsScreen.addObject(nameOfTeam);
            
            speaker1=new JTextField();
            speaker1.setBounds(164,200,150,20);
            cont.add(speaker1);
            speaker1.setVisible(false);
            addTeamsScreen.addObject(speaker1);
            
            speaker2=new JTextField();
            speaker2.setBounds(318,200,150,20);
            cont.add(speaker2);
            speaker2.setVisible(false);
            addTeamsScreen.addObject(speaker2);
            
            speaker3=new JTextField();
            speaker3.setBounds(472,200,150,20);
            cont.add(speaker3);
            speaker3.setVisible(false);
            addTeamsScreen.addObject(speaker3);
            speaker3.addActionListener(this);
            
            speaker4=new JTextField();
            speaker4.setBounds(626,200,150,20);
            speaker4.setVisible(false);
            cont.add(speaker4);
            speaker4.addActionListener(this);
            addTeamsScreen.addObject(speaker4);
            
            LnameOfTeam=new JLabel("Team Name");
            LnameOfTeam.setBounds(10,170,150,20);
            LnameOfTeam.setVisible(false);
            cont.add(LnameOfTeam);
            addTeamsScreen.addObject(LnameOfTeam);
            
            Lspeaker1=new JLabel("Speaker 1");
            Lspeaker1.setBounds(164,170,150,20);
            Lspeaker1.setVisible(false);
            cont.add(Lspeaker1);
            addTeamsScreen.addObject(Lspeaker1);
            
            Lspeaker2=new JLabel("Speaker 2");
            Lspeaker2.setBounds(318,170,150,20);
            Lspeaker2.setVisible(false);
            cont.add(Lspeaker2);
            addTeamsScreen.addObject(Lspeaker2);
            
            Lspeaker3=new JLabel("Speaker 3");
            Lspeaker3.setBounds(472,170,150,20);
            Lspeaker3.setVisible(false);
            cont.add(Lspeaker3);
            addTeamsScreen.addObject(Lspeaker3);
            
            subst=new JCheckBox("Substitution");
            subst.setBounds(626,170,150,20);
            subst.setVisible(false);
            subst.addActionListener(this);
            cont.add(subst);
            addTeamsScreen.addObject(subst);
            
            addTeam=new JButton("Add team");
            addTeam.setBounds(626,250,150,20);
            addTeam.setVisible(false);
            addTeam.addActionListener(this);
            cont.add(addTeam);
            addTeamsScreen.addObject(addTeam);
            
            removeTeam=new JButton("Delete Selected Team");
            removeTeam.setBounds(350,570,200,50);
            removeTeam.setVisible(false);
            removeTeam.addActionListener(this);
            cont.add(removeTeam);
            addTeamsScreen.addObject(removeTeam);
            firstScreen.setVisible(true);
        }
        else
        {
            //JOptionPane.showMessageDialog(null,"tato cast programu nie je dokoncena","nefungujem",JOptionPane.ERROR_MESSAGE);
            //terminate();
            addTeamsFromFile();
            addDebatesFromFile();
            mainScreen.setVisible(true);
            firstRound=false;
            pair.setBounds(450,500,200,50);
            
            tablePanel=new TableDemoPanel(new Object[1][5],new String[]{new String("#"),new String("Team Name"),new String("Wins"),new String("Ballots"),new String("Speakers points")},0);
            tablePanel.setOpaque(true);
            cont.add(tablePanel);
            tablePanel.setBounds(10,200,770,350);
            tablePanel.setVisible(false);         
            tablePanel.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
            tablePanel.getColumnModel().getColumn(0).setMaxWidth(20);
        }  
    }
    public void tableChanged(TableModelEvent e)
    {
        if(e.getSource()==tablePairing.getModel())
        {
            int row=e.getFirstRow();
            int column=e.getColumn();
            if(column==2)
            {
                Debate temp=searchDebate((String)tablePairing.getValueAt(row,0));
                temp.addJudge((String)tablePairing.getValueAt(row,2));
                fileDebates.write(temp.toString(),temp.getIndex());
            }
        }
    }
    public void actionPerformed(ActionEvent e)
    {
        Object source=e.getSource();
        if(source==addResults)
        {
            Object[][] temp=new Object[debates.size()][3];
            for(int i=0;i<debates.size();i++)
            {
                temp[i]=debates.elementAt(i).getResultsRow();
            }
            tableAddingResult.setData(temp);
            mainScreen.setVisible(false);
            addResultsScreen.setVisible(true);
            subtitle.setText("Adding Results");
            end.setText("Back");
        }
        else if(source==teamChart)
        {
            tablePanel.setData(getTeamsData());
            tablePanel.setVisible(true);
            mainScreen.setVisible(false);
            subtitle.setText("Team List");
            end.setText("Back");
        }
        else if(source==speakerChart)
        {
            mainScreen.setVisible(false);
            tableSpeakers.setData(getSpeakersData());
            speakersChartScreen.setVisible(true);
            end.setText("Back");
            subtitle.setText("Speakers List");
        }
        else if(source==end)
        {
            if(end.getText()=="Back")
            {
                if(firstRound)                
                    firstScreen.setVisible(true);               
                else                
                    mainScreen.setVisible(true);                
                addTeamsScreen.setVisible(false);
                pairingScreen.setVisible(false);
                speakersChartScreen.setVisible(false);
                tablePanel.setVisible(false);
                addResultsScreen.setVisible(false);
                results.setVisible(false);
                tableAddingResult.setBounds(10,200,770,350);
                subtitle.setText("Menu");
                end.setText("End");
                pair.setVisible(true);
            }
            else
            {
                Object[]options=new Object[]{new String("finish"),new String("cancel")};
                int result=JOptionPane.showOptionDialog(null, "Do you really want to terminate?","Terminate?",JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
                null,options,options[0]);
                if(result==0)
                    terminate();
            }
        }
        else if(source==pairing)
        {
            mainScreen.setVisible(false);
            pairingScreen.setVisible(true);
            end.setText("Back");
            subtitle.setText("Pairing");
            Object[][]temp=new Object[debates.size()][3];
                for(int i=0;i<debates.size();i++)
                    temp[i]=debates.elementAt(i).getRow();                                    
                tablePairing.setData(temp);
        }
        else if(source==addTeams)
        {
            firstScreen.setVisible(false);
            pairingScreen.setVisible(false);
            addTeamsScreen.setVisible(true);            
            nameOfTeam.requestFocusInWindow();
            if(subst.isSelected())
            {
                speaker4.setVisible(true);
            }
            else
            {
                speaker4.setVisible(false);   
            }
            subtitle.setText("Adding Teams");
            end.setText("Back");
        }
        else if(source==subst)
        {
            if(subst.isSelected())
            {
                speaker4.setVisible(true);
            }
            else
            {
                speaker4.setVisible(false);   
            }
        }
        else if(source==speaker3)
        {
            if(!subst.isSelected())            
                addTeam.doClick();          
        }
        else if(source==speaker4)
        {
            addTeam.doClick();
        }
        else if(source==addTeam)
        {
            String name=nameOfTeam.getText();
            boolean wrong=false;
            if(name.equals("")) wrong=true;       
            String[] speakers;
            if(subst.isSelected())
                speakers=new String[4];
            else    speakers=new String[3];
            speakers[0]=speaker1.getText();
            speakers[1]=speaker2.getText();
            speakers[2]=speaker3.getText();
            if(subst.isSelected())
            {
                if(speaker4.getText().equals(""))
                    wrong=true;
                speakers[3]="N'"+speaker4.getText();
            }
            for(int i=0;i<speakers.length;i++)
                if(speakers[i].equals("")) wrong=true;            
            if(wrong)
            {
                JOptionPane.showMessageDialog(null,"Missing data","error",JOptionPane.ERROR_MESSAGE);
                return;
            }
            else
            {
                addTeam(name,speakers);
                tablePanel.insertRow(teams.elementAt(teams.size()-1).getRow(firstRound));
                speaker1.setText("");
                speaker2.setText("");
                speaker3.setText("");
                speaker4.setText("");
                speaker4.setVisible(false);
                nameOfTeam.setText("");
                subst.setSelected(false);
                nameOfTeam.requestFocusInWindow();
            }            
        }
        else if(source==pair)
        {
            if(firstRound)
            {
                if(teams.size()==0)
                {
                    JOptionPane.showMessageDialog(null,"No Teams Added","Missing Teams",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                Object[] options=new Object[]{new String("prepared"),new String("impro")};
                int imp=JOptionPane.showOptionDialog(null, "Select Type of Thesis", "Thesis",
                JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
                null, options, options[0]);
                boolean impro;
                if(imp==0)impro=false;
                else    impro=true;
                randomPairing(impro);
                
                Object[][]temp=new Object[debates.size()][3];
                for(int i=0;i<debates.size();i++)
                    temp[i]=debates.elementAt(i).getRow();                                    
                tablePairing.setData(temp);
                
                firstScreen.setVisible(false);
                pairingScreen.setVisible(true);
                subtitle.setText("Pairing");
                end.setText("Back");
                firstRound=false;
                tablePanel.setModel(new MyTableModel(new Object[1][5],new String[]{new String("#"),new String("Team Name"),new String("Wins"),new String("Ballots"),new String("Speakers points")},2));
                tablePanel.setBounds(10,200,770,350);
                tablePanel.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
                tablePanel.getColumnModel().getColumn(0).setMaxWidth(20);
                pair.setBounds(450,500,200,50);
            }
            else
            {
                if(!completeResults())
                {
                    JOptionPane.showMessageDialog(null,"Some debate's results are missing", "Results incomplete",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                /*if(!debates.elementAt(debates.size()-1).haveResults())
                {
                    Team temp=searchTeam(debates.elementAt(debates.size()-1).getTeam());
                    averageSP();
                    Speaker [] speakers=temp.getSpeakers();
                    for(int i=0;i<3;i++)
                    {
                        speakers[i].addPoints(averageSpeakerPoints);
                        System.out.println("average s poitns: "+averageSpeakerPoints);
                    }
                    temp.addResults(3);
                    fileTeams.write(temp.toString(),temp.getIndex());
                }*/
                Object[] options=new Object[]{new String("prepared"),new String("impro")};
                int imp=JOptionPane.showOptionDialog(null, "Select Type of Thesis", "Thesis",
                JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
                null, options, options[0]);
                boolean impro;
                if(imp==0)impro=false;
                else    impro=true;
                pair(impro);
                Object[][]temp=new Object[debates.size()][3];
                for(int i=0;i<debates.size();i++)
                    temp[i]=debates.elementAt(i).getRow();
                tablePairing.setData(temp);
                
                mainScreen.setVisible(false);
                pairingScreen.setVisible(true);
                subtitle.setText("Pairing");
                end.setText("Back");                
            }
        }
        else if(source==removeTeam)
        {
            teams.remove(searchTeam(tablePanel.removeRow()));
        }
        else if(source==updateDebate)
        {
            Debate tempDebate=searchDebate((String)tableAddingResult.getValueAtSelected(0));            
            if(tempDebate==null)
                return;
            if(tempDebate.haveResults())
            {
                JOptionPane.showMessageDialog(null,"Results for selected debate already added","results present",JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(tempDebate.getA().equals("")||tempDebate.getN().equals(""))
            {
                JOptionPane.showMessageDialog(null,"Cannot add results to this debate","cannot",JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            Team a=searchTeam(tempDebate.getA());
            Team n=searchTeam(tempDebate.getN());
            Speaker [] temp=a.getSpeakers();
            for(int i=0;i<8;i++)
            {
                speakerResults[i].setVisible(false);
                speakerNames[i].setVisible(false);
            }
            for(int i=0;i<temp.length;i++)
            {
                speakerNames[i].setText(temp[i].getName());
                speakerNames[i].setVisible(true);
                speakerResults[i].setVisible(true);
                speakerResults[i].setText("");
            }
            temp=n.getSpeakers();
            for(int i=4;i<temp.length+4;i++)
            {
                speakerNames[i].setText(temp[i-4].getName());
                speakerNames[i].setVisible(true);
                speakerResults[i].setVisible(true);
                speakerResults[i].setText("");
            }
            teamNameA.setText(tempDebate.getA());
            teamNameN.setText(tempDebate.getN());
            ballotsA.setText("");
            ballotsN.setText("");
            results.setVisible(true);
            tableAddingResult.setBounds(10,200,770,200);
        }
        else if(source==cancelUpdate)
        {
            results.setVisible(false);
            tableAddingResult.setBounds(10,200,770,350);
        }
        else if(source==readDebateUpdate)
        {
            Debate debate=searchDebate((String)tableAddingResult.getValueAtSelected(0));
            Team a=searchTeam(debate.getA());
            Team n=searchTeam(debate.getN());
            if(!correctInput(a,n))
                return;
            int x,y;
            int pointsa=0,pointsn=0;
            try
            {
                x=Integer.parseInt(ballotsA.getText());
                y=Integer.parseInt(ballotsN.getText());
                if(x>=0&&y>=0&&(x+y)==3)
                    debate.addResults(x,y);
                else
                {
                    JOptionPane.showMessageDialog(null,"Invalid number of ballots","Invalid input",JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }catch (NumberFormatException exp)
            {
                JOptionPane.showMessageDialog(null,"Invalid results format","Invalid input",JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            Speaker[] temp=a.getSpeakers();
            y=0; //works as counter of speakers with points (for cases of two speakers)
            for(int i=0;i<temp.length;i++)
            {
                if(!speakerResults[i].getText().trim().equals(""))
                {
                    try
                    {
                        x=Integer.parseInt(speakerResults[i].getText());
                        if(x>=15&&x<=30)
                        {
                            temp[i].addPoints(3*x);
                            y++;
                            pointsa+=3*x;
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null,"Invalid number of speakers points","Invalid input",JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                        if(i==3)
                            a.special++;
                    }catch (NumberFormatException exp)
                    {
                        JOptionPane.showMessageDialog(null,"Invalid results format","Invalid input",JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                }                
            }           
            if(y==2)
            {
                pointsa=(pointsa/2)*3;
            }
            temp=n.getSpeakers();
            y=0;
            for(int i=0;i<temp.length;i++)
            {
                if(!speakerResults[i+4].getText().equals(""))
                {
                    try
                    {
                        x=Integer.parseInt(speakerResults[i+4].getText());
                        if(x>=15&&x<=30)
                        {
                            temp[i].addPoints(3*x);
                            y++;
                            pointsn+=3*x;
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null,"Invalid number of speakers points","Invalid input",JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                        if(i==3)
                            n.special++;
                    }catch (NumberFormatException exp)
                    {
                        JOptionPane.showMessageDialog(null,"Invalid results format","Invalid input",JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                }
            }
            if(y==2)
            {
                pointsn=(pointsn/2)*3;
            }
            a.addBallots(Integer.parseInt(ballotsA.getText()));
            n.addBallots(Integer.parseInt(ballotsN.getText()));
            a.addSpeakersPoints(pointsa);
            n.addSpeakersPoints(pointsn);
            debate.addSpeakersPoints(pointsa+pointsn);
            fileTeams.write(a.toString(),a.getIndex());
            fileTeams.write(n.toString(),n.getIndex());
            fileDebates.write(debate.toString(),debate.getIndex());
            
            if(completeResults())
            {
                if(!debates.elementAt(debates.size()-1).haveResults())
                {
                    Team tempp=searchTeam(debates.elementAt(debates.size()-1).getTeam());
                    averageSP();
                    Speaker [] speakers=tempp.getSpeakers();
                    for(int i=0;i<3;i++)
                    {
                        speakers[i].addPoints(averageSpeakerPoints);
                        System.out.println("average s poitns: "+averageSpeakerPoints);
                    }
                    tempp.addResults(3);
                    fileTeams.write(tempp.toString(),tempp.getIndex());
                    if(debates.elementAt(debates.size()-1).getPosition()==1)                    
                        debates.elementAt(debates.size()-1).addResults(3,0);
                    else
                        debates.elementAt(debates.size()-1).addResults(0,3);
                    fileDebates.write(debates.elementAt(debates.size()-1).toString(),debates.elementAt(debates.size()-1).getIndex());
                    
                    
                    tableAddingResult.setValueAt(debates.size()-1,2,true);
                }
            }
            
            tableAddingResult.setValueAtSelected(2,debate.haveResults());
            cancelUpdate.doClick(); 
        }
    }
    public boolean completeResults()
    {
        for(int i=0;i<debates.size();i++)
        {
            //System.out.println(debates.elementAt(i).haveResults());
            if((!debates.elementAt(i).getA().equals(""))&&(!debates.elementAt(i).getN().equals(""))&&(!debates.elementAt(i).haveResults()))
                return false;
        }
        return true;
    }
    public boolean correctInput(Team a, Team n)
    {
        if(ballotsA.getText().equals("")||ballotsN.getText().equals(""))
        {   
            JOptionPane.showMessageDialog(null,"Number of Ballots missing","Missing data",JOptionPane.ERROR_MESSAGE);
            return false;
        }
        int counter=0;
        for(int i=0;i<4;i++)
        {
            if(speakerResults[i].isVisible()&&(!speakerResults[i].getText().equals("")))                
                counter++;     
        }
        if(counter==4)
        {
            JOptionPane.showMessageDialog(null,"Results for four speakers added","Invalid input",JOptionPane.ERROR_MESSAGE);
            return false;
        }
        else if(counter<3)
        {
            if(counter<2)
            {
                JOptionPane.showMessageDialog(null,"Speakers points missing","Invalid input",JOptionPane.ERROR_MESSAGE);
                return false;
            }
            if(a.special==2)
            {
                JOptionPane.showMessageDialog(null,"Affirmative team cannot debate in incorrect format any more","Invalid input",JOptionPane.ERROR_MESSAGE);
                return false;
            }
            int pass=JOptionPane.showConfirmDialog(null,"Did affirmative team debate with only two speakers?","Two speakers only?",JOptionPane.YES_NO_OPTION);
            if(pass==1)
                return false;
            else
                a.special++;
        }
        counter=0;
        for(int i=0;i<4;i++)
        {
            if(speakerResults[i+4].isVisible()&&(!speakerResults[i+4].getText().equals("")))                
                counter++;     
        }
        if(counter==4)
        {
            JOptionPane.showMessageDialog(null,"Results for four speakers added","Invalid input",JOptionPane.ERROR_MESSAGE);
            return false;
        }
        else if(counter<3)
        {
            if(counter<2)
            {
                JOptionPane.showMessageDialog(null,"Speakers points missing","Invalid input",JOptionPane.ERROR_MESSAGE);
                return false;
            }
            if(n.special==2)
            {
                JOptionPane.showMessageDialog(null,"Negative team cannot debate in incorrect format any more","Invalid input",JOptionPane.ERROR_MESSAGE);
                return false;
            }
            int pass=JOptionPane.showConfirmDialog(null,"Did negative team debate with only two speakers?","Two speakers only?",JOptionPane.YES_NO_OPTION);
            if(pass==1)
                return false;
            else
                n.special++;
        }
        if((!speakerResults[3].getText().equals(""))&&a.special==2)
        {
            JOptionPane.showMessageDialog(null,"Affirmative team cannot debate with substitute any more","Invalid input",JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if((!speakerResults[7].getText().equals(""))&&n.special==2)
        {
            JOptionPane.showMessageDialog(null,"Negative team cannot debate with substitute any more","Invalid input",JOptionPane.ERROR_MESSAGE);
            return false;
        }
        return true;
    }
    public void randomPairing(boolean impro)
    {
        Vector<Team> temp=new Vector<Team>(teams);
        while(temp.size()>1)
        {
            int[] pointer=random(temp);
            Team a=temp.elementAt(pointer[0]);
            Team n=temp.elementAt(pointer[1]);
            temp.remove(a);
            temp.remove(n);
            debates.addElement(new Debate(a,n,debates.size()));
            fileDebates.write(debates.elementAt(debates.size()-1).toString(),debates.elementAt(debates.size()-1).getIndex());
        }
        if(temp.size()!=0)
        {
            debates.addElement(new Debate(temp.elementAt(0),1,debates.size()));
            fileDebates.write(debates.elementAt(debates.size()-1).toString(),debates.elementAt(debates.size()-1).getIndex());
        }
    }
    public void pair(boolean impro) //the algorithm divides sorted teams into groups by numOfWins and creates debates only of teams in the same group
    {
        debates.clear();
        sort();  //uses quick sort to sort vector of teams by number of wins, ballots and speakers' points, respectively
        int start=0;  //coordinate of the first team in a group of pairable teams
        int end=0;      //coordinate of the last team in a group of pairable teams
        int nextend=0;  //these integers are used for marking next group of teams to add to the main group, in order to achieve the same number of negative and affirmative teams in the main group
        int nextstart;
        int a,n;    //number of affirmative and negative teams in the main group
        int max=teams.elementAt(0).nums[0]; //the number of wins of teams in the main group
        Vector<Team>group=new Vector<Team>();   //the main group of teams to pair
        boolean changeOfGroup;
        int min=teams.elementAt(0).nums[0];
        for(int i=1;i<teams.size();i++)
        {
            if(teams.elementAt(i).nums[0]<min)
                min=teams.elementAt(i).nums[0];
        }
        while(max!=min-1)      //till the algorithm does not go through every group
        {
            a=0;
            n=0;
            for(int i=start;i<teams.size();i++)     //adds teams to the main group
            {
                if(teams.elementAt(i).nums[0]==max) //checks whether the particular team is member of the main group
                {
                    group.addElement(teams.elementAt(i));   //add team to the main group
                    //System.out.println(teams.elementAt(i).getName()+"my position: "+teams.elementAt(i).nextDebate(impro));
                    switch(teams.elementAt(i).nextDebate(impro))
                    {
                        case 1:
                            a++;
                            break;
                        case 0:
                            n++;
                            break;                        
                    }
                }
                else
                {
                    end=i-1;
                    break;
                }
            }          
            //System.out.println("MAin group finished   a: "+a+"  n: "+n);
            max--;
            if(a!=n) 
            {            
                int position;   //position that the main group is missing 0-negative, 1-affirmative
                if(a>n)
                    position = 0;
                else
                    position = 1;
                if(max==min-1) //means it is the last group
                {
                    int i;
                    for(i=group.size()-1;group.elementAt(i).nextDebate(impro)==position;i--);    //finds the last team which has not required position           
                    debates.addElement(new Debate(group.elementAt(i),group.remove(i).nextDebate(impro),debates.size()));         
                    
                    fileDebates.write(debates.elementAt(debates.size()-1).toString(),debates.elementAt(debates.size()-1).getIndex());
                    if(position==0)
                        a--;
                    else    n--;
                    if(a!=n)
                        System.out.println("error while equilizing number of  a/n teams in last group: "+a+"/"+n);
                    
                }
                else
                {
                
                    Team []changed=new Team[Math.abs(a-n)]; //records pointers of teams added to the main group
                    int index=0;                //number of teams in the array above               
                    int sign=1;                 //used for alternating selection of teams in next group
                    int counter=1;                                  
                    int pointer;                //points at teams in the next group
                    int nextmax=max;
                    nextstart=end+1;
                    while(true)
                    {
                        changeOfGroup=false;
                        for(pointer=nextstart;pointer<teams.size()&&teams.elementAt(pointer).nums[0]==nextmax;pointer++);
                        nextend=pointer-1;  //sets index of the last team in the next group
                        pointer=(end+1+nextend)/2;  //sets pointer to a team in the middle of the next group
                        while(a!=n)
                        {
                            if(teams.elementAt(pointer).nextDebate(impro)==position)    //check for the proper position
                            {
                                group.addElement(teams.elementAt(pointer));
                                System.out.println(teams.elementAt(pointer).getName());
                                changed[index]=teams.elementAt(pointer);
                                index++;
                                if(position==0)
                                    n++;
                                else
                                    a++;
                            }
                            pointer=pointer+sign*counter;
                            sign=-sign;
                            counter++;
                            if(pointer>nextend) //the pointer have got beyond the next group,, in this case, new next group has to be worked on
                            {
                                changeOfGroup=true; //
                                break;  
                            }//use flag, two loops inside each other, flag for breaking the outside loop
                        }
                        if(changeOfGroup)   //sets the variables for another next group
                        {
                            nextmax--;
                            nextstart=nextend+1;
                            sign=1;
                            counter=1;
                        }
                        else     //this means the above loop has ended because there is the same number of A and N teams in the main group,, the group can be finished
                        {
                            for(int i=0;i<index;i++)
                            {
                                teams.remove(changed[i]);
                                teams.insertElementAt(changed[i],end+1);
                                end++;
                            }
                            break;
                        }
                    }
                }
            }//the main group is complete, the debates can be created
            start=end+1;
            //System.out.println("check for odd group size: "+group.size()+"   max= "+max);
            if(group.size()%2!=0&&max!=min-1)
            {
                int pointer;
                for(pointer=end+1;pointer<teams.size()&&teams.elementAt(pointer).nums[0]==max;pointer++);
                nextend=pointer-1;  //sets index of the last team in the next group
                pointer=(end+1+nextend)/2;
                //System.out.println("Transfer timu, start: "+(end+1)+"   end: "+nextend+"   position: "+pointer);
                //System.out.println(teams.elementAt(pointer).getName());
                group.addElement(teams.elementAt(pointer));
                teams.insertElementAt(teams.remove(pointer),end+1);
                start++;
            }
            //System.out.println("Idem poparovat group: "+group.size());
            while(group.size()!=0)
            {
                //System.out.println("group size: "+group.size());
                if(group.size()==1)
                {
                    debates.addElement(new Debate(group.elementAt(0),group.remove(0).nextDebate(impro),debates.size()));
                    fileDebates.write(debates.elementAt(debates.size()-1).toString(),debates.size()-1);                    
                }
                else
                {
                    int i;
                    for(i=0;group.elementAt(i).nextDebate(impro)==0;i++);    //find first affirmative team
                        a=i;
                    for(i=group.size()-1;group.elementAt(i).nextDebate(impro)==1;i--);   //the last negative team
                        n=i;
                    //System.out.println("a: "+a+"   n: "+n);
                    Team temp=group.elementAt(a);
                    debates.addElement(new Debate(temp,group.remove(n),debates.size()));    //the teams are removed from group,, the debate is created
                    group.remove(temp);
                    fileDebates.write(debates.elementAt(debates.size()-1).toString(),debates.elementAt(debates.size()-1).getIndex());
                }
            }
            group.clear();
        }
        updatePositions(impro);
    }
    public void sort()
    {
        max(0,teams.size()-1,2);
        max(0,teams.size()-1,1);
        max(0,teams.size()-1,0);
    }    
    public void max(int start, int end, int index)
    {        
        for(int i=1;i<teams.size();i++)
        {
            for(int j=0;j<i;j++)
            {
                if(teams.elementAt(j).nums[index]<teams.elementAt(i).nums[index])
                {
                    teams.insertElementAt(teams.remove(i),j);
                    break;
                }
            }
        }
    }
    public void addTeamsFromFile()
    {
        for(int i=0;i<fileTeams.getSize();i++)     
        {
            teams.addElement(new Team(fileTeams.read(i),i));
            Speaker[]tempp=teams.elementAt(teams.size()-1).getSpeakers();
            for(int j=0;j<tempp.length;j++)
                speakers.addElement(tempp[j]);
        }
    }
    public void addDebatesFromFile()
    {
        for(int i=0;i<fileDebates.getSize();i++)
        {
            debates.addElement(new Debate(fileDebates.read(i),i));
        }
    }
    public void addTeam(String name,String []s)
    {
        Team temp=new Team(name,s,teams.size());
        teams.addElement(temp);        
        fileTeams.write(temp.toString(),fileTeams.getNumOfRecords());
        Speaker[]tempp=teams.elementAt(teams.size()-1).getSpeakers();
        for(int i=0;i<tempp.length;i++)
            speakers.addElement(tempp[i]);
    }    
    public Team searchTeam(String name)
    {
        for(int i=0;i<teams.size();i++)
        {
            if(teams.elementAt(i).nameOfTeam.equals(name))
                return teams.elementAt(i);
        }
        return null;
    }
    public Speaker searchSpeaker(String id)
    {
        for(int i=0;i<speakers.size();i++)
        {
            if(speakers.elementAt(i).getID().equals(id))
            {
                return speakers.elementAt(i);
            }           
        }
        return null;
    }
    public void addResults(Debate d, int ballots1, int ballots2, String [] speakers, int [] points)
    {        
        searchSpeaker(""+d.getA()+speakers[0]).addPoints(points[0]);        
        searchSpeaker(""+d.getA()+speakers[1]).addPoints(points[1]);
        searchSpeaker(""+d.getA()+speakers[2]).addPoints(points[2]);
        searchSpeaker(""+d.getN()+speakers[3]).addPoints(points[3]);
        searchSpeaker(""+d.getN()+speakers[4]).addPoints(points[4]);
        searchSpeaker(""+d.getN()+speakers[5]).addPoints(points[5]);
        searchTeam(d.getA()).addResults(ballots1);
        searchTeam(d.getN()).addResults(ballots2);
        d.addResults(ballots1,ballots2);
    }
    public void addResults(String teamA,int ballots1, int ballots2, String[]speakers,int[]points)
    {
        Debate d=searchDebate(teamA);
        searchSpeaker(""+d.getA()+speakers[0]).addPoints(points[0]*ballots1);        
        searchSpeaker(""+d.getA()+speakers[1]).addPoints(points[1]);
        searchSpeaker(""+d.getA()+speakers[2]).addPoints(points[2]);
        searchSpeaker(""+d.getN()+speakers[3]).addPoints(points[3]);
        searchSpeaker(""+d.getN()+speakers[4]).addPoints(points[4]);
        searchSpeaker(""+d.getN()+speakers[5]).addPoints(points[5]);
        searchTeam(d.getA()).addResults(ballots1);
        searchTeam(d.getN()).addResults(ballots2);
        d.addResults(ballots1,ballots2);
    }
    public Debate searchDebate(String teamA)
    {
        for(int i=0;i<debates.size();i++)
        {
            if(debates.elementAt(i).getA().equals(teamA))
                return debates.elementAt(i);
        }
        return null;
    }    
    public void displayTeams()
    {
        System.out.println("teams: ");
        for(int i=0;i<teams.size();i++)
        {
            int []temp=teams.elementAt(i).getNums();
            System.out.println(""+(i+1)+". "+teams.elementAt(i).getName()+"\t "+temp[0]+"\t"+temp[1]+"\t"+temp[2]);
        }
    }
    public void displayPairing()
    {
        System.out.println("pairing: ");
        for(int i=0;i<debates.size();i++)
        {
            System.out.println(""+debates.elementAt(i).getA()+"\t vs. \t"+debates.elementAt(i).getN());
        }
    }
    public void displayGroup(Vector<Team> g)
    {
        System.out.println("group to pair: ");
        for(int i=0;i<g.size();i++)
        {
            int []temp=g.elementAt(i).getNums();
            System.out.println(""+(i+1)+". "+g.elementAt(i).getName()+"\t "+temp[0]+"\t"+temp[1]+"\t"+temp[2]);
        }
    }
    public int[]random(Vector<Team>v)
    {
        int []temp=new int[2];
        temp[0]=(int)(Math.random()*v.size());
        temp[1]=(int)(Math.random()*v.size());
        while(temp[0]==temp[1])
            temp[1]=(int)(Math.random()*v.size());
        return temp;
    }
    public void terminate()
    {
        fileTeams.close();
        System.exit(0);
    }
    public static void main()
    {
        GUI g=new GUI();
        for(int i=0;i<8;i++)
            g.addTeam(new String("Team"+i),new String[]{new String("A"),new String("B"),new String("C")});
    }
    public Object[][] getSpeakersData()
    {
        sortSpeakers(0,speakers.size()-1);
        Object[][]temp=new Object[speakers.size()][3];
        for(int i=0;i<speakers.size();i++)
        {
            Object[]tempp=speakers.elementAt(i).getRow();
            temp[i]=new Object[]{new String(""+(i+1)),tempp[0],tempp[1],tempp[2]};
        }
        return temp;
    }
    public void sortSpeakers(int start, int end)
    {
        int pivot=((speakers.elementAt(start).getPoints()+speakers.elementAt(end).getPoints())/2);
        int left=start;
        int right=end;
        while(left<=right)
        {
            while(speakers.elementAt(left).getPoints()>pivot)
                left++;
            while(speakers.elementAt(right).getPoints()<pivot)
                right--;
            if(left<=right)
            {
                Collections.swap(speakers,left,right);
                left++;
                right--;
            }
        }
        if(start<right)
            sortSpeakers(start,right);
        if(left<end)
            sortSpeakers(left,end);
    }
    public Object[][] getTeamsData()
    {
        sort();
        Object[][]temp=new Object[teams.size()][5];
        
        for(int i=0;i<teams.size();i++)
        {
            Object[]tempp=teams.elementAt(i).getRow(firstRound);
            temp[i]=new Object[]{i+1,tempp[0],tempp[1],tempp[2],tempp[3]};
        }
        return temp;
    }
    public void writeExcel()
    {
        excelWriter=new JavaExcelWrite("jano");
        excelWriter.addSheet("teams1",new String[]{new String("#"),new String("Team name"),new String("Wins"),new String("Ballots"),new String("Speakers points")},getTeamsData());
    }
    public void averageSP()
    {
        int sum=0;
        for(int i=0;i<debates.size()-1;i++)
        {
            sum+=debates.elementAt(i).getSpeakersPoints();
        }
        averageSpeakerPoints=sum/((debates.size()-1)*6);
    }
    public void updatePositions(boolean impro)
    {
        Debate temp;
        Team a,n;
        for(int i=0;i<debates.size();i++)
        {
            temp=debates.elementAt(i);
            a=searchTeam(temp.getA());
            if(a!=null)
            {
                a.setPosition('n',impro);
                fileTeams.write(a.toString(),a.getIndex());
            }
            n=searchTeam(temp.getN());
            if(n!=null)
            {
                n.setPosition('a',impro);
                fileTeams.write(n.toString(),n.getIndex());
            }
        }
    }
    public static void main(String [] args){
        new GUI();
    }
}