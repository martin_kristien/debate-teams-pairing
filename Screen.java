import java.util.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
public class Screen
{
    Vector<JComponent>objects;
    public Screen()
    {
        objects=new Vector<JComponent>();
    }
    public void addObject(Object o)
    {
        if(o instanceof JComponent)
        {
            objects.addElement((JComponent)o);
        }
    }
    public void setVisible(boolean visible)
    {
        for(int i=0;i<objects.size();i++)
        {
            objects.elementAt(i).setVisible(visible);
        }
    }
}