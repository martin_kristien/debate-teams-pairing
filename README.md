# Debate Tournaments Pairing

This application is GUI based system for managing debate tournaments. The application allows user to create new teams when new tournament is started, or to add results of debates and generate pairing for next debate rounds for already started tournaments.


# Runnning
compile the application by:
`javac GUI.java`

run the application by:
`java GUI`

The application was developed on a Windows machine running java and javac versions 1.7.

# Author
The application was developed as part of Computer Science in International Baccalaureate at Gymnaziu Jura Hronca, Bratislava, Slovakia. It was developed by *Martin Kristien* in 2013. Some parts of the application may not have been successfully translated to English (the original language was Slovak).
