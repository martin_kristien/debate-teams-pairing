class Speaker
{
    String id;
    String name;
    int numOfPoints;
    public Speaker(String name,String nameOfTeam)
    {
        id=new String(""+nameOfTeam+name);
        this.name=name;        
    }
    public Speaker(String data, String nameOfTeam, int numOfPoints)
    {
        this.name=data;
        id=new String(""+nameOfTeam+name);
        this.numOfPoints=numOfPoints;
    }
    public String getName()
    {
        return name;
    }
    public String getID()
    {
        return id;
    }
    public String getTeamName()
    {
        for(int i=0;i<id.length();i++)
        {
            if(id.substring(i).equals(name))
                return id.substring(0,i);
        }
        return null;
    }
    public int getPoints()
    {
        return numOfPoints;
    }
    public void addPoints(int points)
    {
        numOfPoints+=points;
    }
    public String toString()
    {
        return new String (""+name+";"+numOfPoints);
    }
    public Object[]getRow()
    {
        Object[] temp=new Object[]{name, numOfPoints, getTeamName()};
        return temp;
    }
}