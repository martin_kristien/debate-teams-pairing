import javax.swing.*;
import java.awt.*;
import javax.swing.table.*;
public class TableDemoPanel extends JPanel
{
    private JTable table;
    public MyTableModel model;
    public TableDemoPanel(Object[][] data,String[] columnNames, int type)
    {
        super(new BorderLayout());
        model=new MyTableModel(data,columnNames,type);
        table=new JTable(model);
        
        table.setPreferredScrollableViewportSize(new Dimension(500,200));
        table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        JScrollPane scrollPane=new JScrollPane(table);
        add(scrollPane);
    }
    public void setAutoResizeMode(int mode)
    {
        table.setAutoResizeMode(mode);
    }
    public TableColumnModel getColumnModel()
    {
        return table.getColumnModel();
    }
    public void setModel(MyTableModel model)
    {
        this.model=model; 
        table.setModel(model);
    }
    public int getSelectedRow()
    {
        int row=table.getSelectionModel().getLeadSelectionIndex();
        return row;
    }
    public int getAbsRow(int row)
    {
        return table.convertRowIndexToModel(row);
    }
    public Object[]getRowValues(int row)
    {
        int absRow=getAbsRow(row);
        return model.getRowValues(absRow);
    }
    public void setEnabled(boolean b)
    {
        table.setEnabled(b);
        table.setRowSelectionAllowed(b);
    }
    public void insertRow(Object[] row)
    {
        model.insertRow(row);
        model.fireTableRowsInserted(0,0);
    }
    public Object getValueAtSelected(int column)
    {
        int row=table.getSelectedRow();
        if(row<0)
        {
            JOptionPane.showMessageDialog(null,"Označ debatu","Chýba označenie",JOptionPane.ERROR_MESSAGE);
            return null;
        }
        return model.getValueAt(row, column);
    }
    public String removeRow()
    {
        int row=table.getSelectedRow();
        if(row==model.getRowCount()-1)
            return null;
        if(row<0)
        {
            JOptionPane.showMessageDialog(null,"Označ tím, ktorý chceš vymazať","Chýba označenie",JOptionPane.ERROR_MESSAGE);
            return null;
        }
        int answer=JOptionPane.showConfirmDialog(null,"Si si istý, že chceš vymazať označený tím?","U Sure?",JOptionPane.YES_NO_OPTION);
        if(answer==1)
            return null;
        String temp=(String)model.getValueAt(row,0);
        model.removeRow(row);
        return temp;
    }
    public void setColumnSelection(int start, int end)
    {
        table.setColumnSelectionInterval(start,end);
    }
    public void setRowValues(int r, Object[] row)
    {
        r=getAbsRow(r);
        model.setRowValues(r,row);        
    }
    public void setData(Object[][]newdata)
    {
        model.setData(newdata);
    }
    public MyTableModel getModel()
    {
        return model;
    }
    public Object getValueAt(int r, int c)
    {
        return model.getValueAt(r,c);
    }
    public void setValueAtSelected(int collumn, Object value)
    {
        int row=table.getSelectedRow();
        table.setValueAt(value,row,collumn);
    }
    public void setValueAt(int row, int col, Object data)
    {
        table.setValueAt(data,row,col);
    }
}